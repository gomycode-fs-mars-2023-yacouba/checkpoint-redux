import React, { useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { addProfesseur } from './actions/professeurActions';
import './professeurs.css';

const Professeurs = () => {
  const dispatch = useDispatch();
  const [professeur, setProfesseur] = useState({
    id: '',
    nom: '',
    prenom: '',
    matiere: '',
    email: '',
  });

  const handleChange = (e) => {
    setProfesseur({ ...professeur, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    professeur.id = generateUniqueId();
    dispatch(addProfesseur(professeur));
    setProfesseur({
      id: '',
      nom: '',
      prenom: '',
      matiere: '',
      email: '',
    });
  };

  const generateUniqueId = () => {
    const uniqueId = 'professeur-' + Math.floor(Math.random() * 10000);
    return uniqueId;
  };

  return (
    <Container>
      <div className="mt-2">
        <h2 className="text-center">Ajouter un professeur</h2>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="nom">
            <Form.Label>Nom</Form.Label>
            <Form.Control
              type="text"
              name="nom"
              value={professeur.nom}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="prenom">
            <Form.Label>Prénom</Form.Label>
            <Form.Control
              type="text"
              name="prenom"
              value={professeur.prenom}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="matiere">
            <Form.Label>Matière</Form.Label>
            <Form.Control
              type="text"
              name="matiere"
              value={professeur.matiere}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              name="email"
              value={professeur.email}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Ajouter
          </Button>
        </Form>
      </div>
    </Container>
  );
};

export default Professeurs;
