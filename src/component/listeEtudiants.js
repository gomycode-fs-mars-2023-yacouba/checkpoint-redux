import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Table, Button, Modal, Form, Container } from 'react-bootstrap';
import { deleteEtudiant, fetchEtudiants, modifyEtudiant } from './actions/etudiantActions';

const ListeEtudiants = ({ etudiants, fetchEtudiants, deleteEtudiant, modifyEtudiant }) => {
  useEffect(() => {
    fetchEtudiants();
  }, [fetchEtudiants]);

  const [showModal, setShowModal] = useState(false);
  const [selectedEtudiant, setSelectedEtudiant] = useState(null);
  const [editedNom, setEditedNom] = useState('');
  const [editedPrenom, setEditedPrenom] = useState('');
  const [editedNote, setEditedNote] = useState('');
  const [editedMatiere, setEditedMatiere] = useState('');
  const [editedFiliere, setEditedFiliere] = useState('');
  const [editedCoefficient, setEditedCoefficient] = useState('');
  const [searchTerm, setSearchTerm] = useState('');

  const handleDelete = (id) => {
    const confirmDelete = window.confirm('Voulez-vous vraiment supprimer cet étudiant ?');
    if (confirmDelete) {
      deleteEtudiant(id);
    }
  };

  const handleEdit = (etudiant) => {
    setSelectedEtudiant(etudiant);
    setEditedNom(etudiant.nom);
    setEditedPrenom(etudiant.prenom);
    setEditedNote(etudiant.note);
    setEditedMatiere(etudiant.matiere);
    setEditedFiliere(etudiant.filiere);
    setEditedCoefficient(etudiant.coefficient);
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setSelectedEtudiant(null);
    setEditedNom('');
    setEditedPrenom('');
    setEditedNote('');
    setEditedMatiere('');
    setEditedFiliere('');
    setEditedCoefficient('');
  };

  const handleSaveChanges = () => {
    const updatedEtudiant = {
      ...selectedEtudiant,
      nom: editedNom,
      prenom: editedPrenom,
      note: editedNote,
      matiere: editedMatiere,
      filiere: editedFiliere,
      coefficient: editedCoefficient,
    };

    modifyEtudiant(updatedEtudiant);
    handleCloseModal();
  };

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  const filteredEtudiants = etudiants.filter((etudiant) =>
    etudiant.nom.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <Container>
      <h2 className="text-center mt-4 mb-3">Liste des étudiants</h2>
      <div className="d-flex justify-content-center mb-3">
        <Form.Group controlId="search">
          <Form.Control
            type="text"
            placeholder="Rechercher un étudiant"
            value={searchTerm}
            onChange={handleSearch}
            size="sm"
          />
        </Form.Group>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th className="text-center">ID</th>
            <th className="text-center">Nom</th>
            <th className="text-center">Prénom</th>
            <th className="text-center">Note</th>
            <th className="text-center">Matière</th>
            <th className="text-center">Filière</th>
            <th className="text-center">Coefficient</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredEtudiants.map((etudiant, index) => (
            <tr key={etudiant.id}>
              <td className="text-center">{index + 1}</td>
              <td className="text-center">{etudiant.nom}</td>
              <td className="text-center">{etudiant.prenom}</td>
              <td className="text-center">{etudiant.note}</td>
              <td className="text-center">{etudiant.matiere}</td>
              <td className="text-center">{etudiant.filiere}</td>
              <td className="text-center">{etudiant.coefficient}</td>
              <td className="text-center">
                <Button variant="danger" onClick={() => handleDelete(etudiant.id)} className="mr-2">
                  Supprimer
                </Button>{' '}
                <Button variant="info" onClick={() => handleEdit(etudiant)}>
                  Modifier
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Modifier l'étudiant</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Nom</Form.Label>
              <Form.Control type="text" value={editedNom} onChange={(e) => setEditedNom(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Prénom</Form.Label>
              <Form.Control type="text" value={editedPrenom} onChange={(e) => setEditedPrenom(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Note</Form.Label>
              <Form.Control type="text" value={editedNote} onChange={(e) => setEditedNote(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Matière</Form.Label>
              <Form.Control type="text" value={editedMatiere} onChange={(e) => setEditedMatiere(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Filière</Form.Label>
              <Form.Control type="text" value={editedFiliere} onChange={(e) => setEditedFiliere(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Coefficient</Form.Label>
              <Form.Control
                type="text"
                value={editedCoefficient}
                onChange={(e) => setEditedCoefficient(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Annuler
          </Button>
          <Button variant="primary" onClick={handleSaveChanges}>
            Enregistrer
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
};

const mapStateToProps = (state) => ({
  etudiants: state.etudiant.etudiants,
});

const mapDispatchToProps = {
  fetchEtudiants,
  deleteEtudiant,
  modifyEtudiant,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListeEtudiants);
