import React, { useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { addEtudiant } from './actions/etudiantActions';

const Etudiants = () => {
  const dispatch = useDispatch();
  const [etudiant, setEtudiant] = useState({
    id: '',
    nom: '',
    prenom: '',
    note: '',
    matiere: '',
    filiere: '',
    coefficient: '',
  });

  const handleChange = (e) => {
    setEtudiant({ ...etudiant, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    etudiant.id = generateUniqueId(); // Assure une clé unique pour chaque étudiant
    dispatch(addEtudiant(etudiant));
    setEtudiant({
      id: '',
      nom: '',
      prenom: '',
      note: '',
      matiere: '',
      filiere: '',
      coefficient: '',
    });
  };

  // Fonction pour générer un identifiant unique (exemple : "etudiant-12345")
  const generateUniqueId = () => {
    const uniqueId = 'etudiant-' + Math.floor(Math.random() * 10000);
    return uniqueId;
  };

  return (
    <Container>
      <div className="mt-10">
        <h2 className="text-center">Ajouter un étudiant</h2>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="nom">
            <Form.Label>Nom</Form.Label>
            <Form.Control
              type="text"
              name="nom"
              value={etudiant.nom}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="prenom">
            <Form.Label>Prénom</Form.Label>
            <Form.Control
              type="text"
              name="prenom"
              value={etudiant.prenom}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="note">
            <Form.Label>Note</Form.Label>
            <Form.Control
              type="text"
              name="note"
              value={etudiant.note}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="matiere">
            <Form.Label>Matière</Form.Label>
            <Form.Control
              type="text"
              name="matiere"
              value={etudiant.matiere}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <Form.Group controlId="filiere">
            <Form.Label>Filière</Form.Label>
            <Form.Control
              type="text"
              name="filiere"
              value={etudiant.filiere}
              onChange={handleChange}
              required
            />
          </Form.Group>
          <div className="mb-3">
            <Form.Group controlId="coefficient">
              <Form.Label>Coefficient</Form.Label>
              <Form.Control
                type="text"
                name="coefficient"
                value={etudiant.coefficient}
                onChange={handleChange}
                required
              />
            </Form.Group>
          </div>
          <div className="mb-3">
            <Button variant="primary" type="submit" className="mx-auto d-block">
              Ajouter
            </Button>
          </div>
        </Form>
      </div>
    </Container>
  );
};

export default Etudiants;
