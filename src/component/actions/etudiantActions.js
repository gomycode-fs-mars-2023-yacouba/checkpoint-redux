import { AJOUTER_ETUDIANT, SUPPRIMER_ETUDIANT, MODIFIER_ETUDIANT } from './types';

export const fetchEtudiants = () => {
  return (dispatch) => {
    // Votre logique pour récupérer les étudiants depuis votre API ou source de données
    // Assurez-vous d'envoyer l'action FETCH_ETUDIANTS avec les étudiants récupérés

    // Placeholder for your logic to fetch students from an API or data source

    // Example of dispatching the FETCH_ETUDIANTS action with retrieved students
    // dispatch({
    //   type: FETCH_ETUDIANTS,
    //   payload: etudiants,
    // });
  };
};

export const addEtudiant = (etudiant) => {
  return {
    type: AJOUTER_ETUDIANT,
    payload: etudiant,
  };
};

export const deleteEtudiant = (id) => {
  return {
    type: SUPPRIMER_ETUDIANT,
    payload: id,
  };
};

export const modifyEtudiant = (etudiant) => {
  return {
    type: MODIFIER_ETUDIANT,
    payload: etudiant,
  };
};
