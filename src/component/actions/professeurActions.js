import { AJOUTER_PROFESSEUR, SUPPRIMER_PROFESSEUR, MODIFIER_PROFESSEUR } from './typesProfesseur';

export const fetchProfesseurs = () => {
  return (dispatch) => {
    // Votre logique pour récupérer les professeurs depuis votre API ou source de données
    // Assurez-vous d'envoyer l'action FETCH_PROFESSEURS avec les professeurs récupérés

    // Placeholder for your logic to fetch professors from an API or data source

    // Example of dispatching the FETCH_PROFESSEURS action with retrieved professors
    // dispatch({
    //   type: FETCH_PROFESSEURS,
    //   payload: professeurs,
    // });
  };
};

export const addProfesseur = (professeur) => {
  return {
    type: AJOUTER_PROFESSEUR,
    payload: professeur,
  };
};

export const deleteProfesseur = (id) => {
  return {
    type: SUPPRIMER_PROFESSEUR,
    payload: id,
  };
};

export const modifyProfesseur = (professeur) => {
  return {
    type: MODIFIER_PROFESSEUR,
    payload: professeur,
  };
};
