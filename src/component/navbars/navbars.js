import React, { Fragment } from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import { Outlet, Link } from "react-router-dom";
import "./navbars.css";

const Navbars = () => {
  return (
    <>
      <Navbar className="nav bg-dark text-white">
        <Container>
          <Navbar.Brand className="nav bg-bgdark " href="#home">
          Ecole Privée Le - <span>  SONGHOÏ  </span>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="navlink ms-auto">
              <Nav.Link as={Link} to="/">
                Home
              </Nav.Link>
              <Nav.Link as={Link} to="/etudiants">
               Ajouter un Etudiant
              </Nav.Link>
              <Nav.Link as={Link} to="/professeurs">
               ajouter un  Professeurs
              </Nav.Link>
              <NavDropdown title="Dropdow" id="basic-nav-dropdown " className="drop bg-dark"> 
                <NavDropdown.Item as={Link} to="/notes">
                  Note
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/coefficients">
                  Coefficients
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/filiere">
                  Filiere
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/matieres">
                  Matieres
                </NavDropdown.Item>
                <NavDropdown.Divider className=" " />
                <NavDropdown.Item as={Link} to="/listeEtudiants">
                  Listes des Edudiants
                </NavDropdown.Item>
                <NavDropdown.Item as={Link} to="/listeProfesseurs">
                  Listes des Professeurs
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <section>
        <Outlet> </Outlet>
      </section>
    </>
  );
};

export default Navbars;
