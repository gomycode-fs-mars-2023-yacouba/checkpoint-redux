import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Table, Button, Modal, Form, Container } from 'react-bootstrap';
import { deleteProfesseur, fetchProfesseurs, modifyProfesseur } from './actions/professeurActions';


const ListeProfesseurs = ({ professeurs, fetchProfesseurs, deleteProfesseur, modifyProfesseur }) => {
  useEffect(() => {
    fetchProfesseurs();
  }, [fetchProfesseurs, modifyProfesseur]);

  const [showModal, setShowModal] = useState(false);
  const [selectedProfesseur, setSelectedProfesseur] = useState(null);
  const [editedNom, setEditedNom] = useState('');
  const [editedPrenom, setEditedPrenom] = useState('');
  const [editedEmail, setEditedEmail] = useState('');
  const [editedMatiere, setEditedMatiere] = useState('');
  const [searchTerm, setSearchTerm] = useState('');

  const handleDelete = (id) => {
    const confirmDelete = window.confirm('Voulez-vous vraiment supprimer ce professeur ?');
    if (confirmDelete) {
      deleteProfesseur(id);
    }
  };

  const handleEdit = (professeur) => {
    setSelectedProfesseur(professeur);
    setEditedNom(professeur.nom);
    setEditedPrenom(professeur.prenom);
    setEditedEmail(professeur.email);
    setEditedMatiere(professeur.matiere);
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setSelectedProfesseur(null);
    setEditedNom('');
    setEditedPrenom('');
    setEditedEmail('');
    setEditedMatiere('');
  };

  const handleSaveChanges = () => {
    const updatedProfesseur = {
      ...selectedProfesseur,
      nom: editedNom,
      prenom: editedPrenom,
      email: editedEmail,
      matiere: editedMatiere,
    };

    modifyProfesseur(updatedProfesseur);
    handleCloseModal();
  };

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  const filteredProfesseurs = professeurs.filter((professeur) =>
    `${professeur.nom} ${professeur.prenom}`.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <Container className="transparent-container">
      <h2 className="text-center mt-4 mb-3">Liste des professeurs</h2>
      <div className="mb-3 d-flex justify-content-center">
        <Form.Group controlId="search" className="w-50">
          <Form.Control
            type="text"
            placeholder="Rechercher un professeur"
            value={searchTerm}
            onChange={handleSearch}
            size="sm"
          />
        </Form.Group>
      </div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th className="text-center">ID</th>
            <th className="text-center">Nom</th>
            <th className="text-center">Prénom</th>
            <th className="text-center">Email</th>
            <th className="text-center">Matière</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredProfesseurs.map((professeur, index) => (
            <tr key={professeur.id}>
              <td className="text-center">{index + 1}</td>
              <td className="text-center">{professeur.nom}</td>
              <td className="text-center">{professeur.prenom}</td>
              <td className="text-center">{professeur.email}</td>
              <td className="text-center">{professeur.matiere}</td>
              <td className="text-center">
                <Button variant="primary" onClick={() => handleEdit(professeur)} className="mr-2">
                  Modifier
                </Button>
                <Button variant="danger" onClick={() => handleDelete(professeur.id)}>
                  Supprimer
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Modifier le professeur</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Nom</Form.Label>
              <Form.Control type="text" value={editedNom} onChange={(e) => setEditedNom(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Prénom</Form.Label>
              <Form.Control type="text" value={editedPrenom} onChange={(e) => setEditedPrenom(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type="text" value={editedEmail} onChange={(e) => setEditedEmail(e.target.value)} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Matière</Form.Label>
              <Form.Control type="text" value={editedMatiere} onChange={(e) => setEditedMatiere(e.target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Annuler
          </Button>
          <Button variant="primary" onClick={handleSaveChanges}>
            Enregistrer
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
};

const mapStateToProps = (state) => ({
  professeurs: state.professeur.professeurs,
});

const mapDispatchToProps = {
  fetchProfesseurs,
  deleteProfesseur,
  modifyProfesseur,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListeProfesseurs);
