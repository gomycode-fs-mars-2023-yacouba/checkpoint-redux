import React from 'react';
import {Carousel } from 'react-bootstrap';
import './home.css'


const Home = () => {
    return (
        <div className="">
 
    <Carousel slide={false}>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.create.vista.com/api/media/medium/466114624/stock-photo-african-american-graduate-hugging-asian?token="
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>WELCOME</h3>
          <p>Ecole Fondamentale Privée le Songhoï</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.create.vista.com/api/media/small/466376816/stock-photo-international-group-of-students-having"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.create.vista.com/api/media/medium/466114624/stock-photo-african-american-graduate-hugging-asian?token="
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>



        </div>
    );
}

export default Home;
