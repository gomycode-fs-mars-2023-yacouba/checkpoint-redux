// etudiantReducer.js

const initialState = {
  etudiants: []
};

const etudiantReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'AJOUTER_ETUDIANT':
      return {
        ...state,
        etudiants: [...state.etudiants, action.payload]
      };
    case 'SUPPRIMER_ETUDIANT':
      return {
        ...state,
        etudiants: state.etudiants.filter(etudiant => etudiant.id !== action.payload)
      };
    case 'MODIFIER_ETUDIANT':
      return {
        ...state,
        etudiants: state.etudiants.map(etudiant =>
          etudiant.id === action.payload.id ? action.payload : etudiant
        )
      };
    default:
      return state;
  }
};

export default etudiantReducer;
