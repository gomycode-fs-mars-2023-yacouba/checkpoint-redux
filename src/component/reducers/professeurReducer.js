const initialState = {
  professeurs: [], // Assurez-vous que la propriété est correctement définie avec une valeur par défaut
};

const professeurReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'AJOUTER_PROFESSEUR':
      return {
        ...state,
        professeurs: [...state.professeurs, action.payload]
      };
    case 'SUPPRIMER_PROFESSEUR':
      return {
        ...state,
        professeurs: state.professeurs.filter(professeur => professeur.id !== action.payload)
      };
    case 'MODIFIER_PROFESSEUR':
      return {
        ...state,
        professeurs: state.professeurs.map(professeur =>
          professeur.id === action.payload.id ? action.payload : professeur
        )
      };
    default:
      return state;
  }
};

export default professeurReducer;
