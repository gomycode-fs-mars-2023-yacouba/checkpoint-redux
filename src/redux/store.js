import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk'; // Importez le middleware redux-thunk
import etudiantReducer from '../component/reducers/etudiantReducer';
import professeurReducer from '../component/reducers/professeurReducer';

const rootReducer = combineReducers({
  etudiant: etudiantReducer,
  professeur: professeurReducer,
  // Ajoutez d'autres reducers ici
});

const store = createStore(rootReducer, applyMiddleware(thunk)); // Appliquez le middleware redux-thunk

export default store;
