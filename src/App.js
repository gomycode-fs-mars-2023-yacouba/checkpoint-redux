import "bootstrap/dist/css/bootstrap.min.css";

import { Navigate, Route, Routes } from "react-router-dom";

import store from "./redux/store";

import Home from "./component/home";

import Etudiants from "./component/etudiants";
import Navbars from "./component/navbars/navbars";
import Notes from "./component/notes";
import Coefficients from "./component/coefficients";
import Filieres from "./component/filieres";
import Matieres from "./component/matieres";
import ListeEtudiants from "./component/listeEtudiants";
import { Provider } from "react-redux";
import Professeurs from "./component/professeurs";
import ListeProfesseurs from "./component/listeProfesseurs";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Routes>
          <Route path="/" element={<Navbars />}>
            <Route index element={<Home />} />

            <Route path="/etudiants" element={<Etudiants />} />
            <Route path="/professeurs" element={<Professeurs />} />
            <Route path="/notes" element={<Notes />} />
            <Route path="/Coefficients" element={<Coefficients />} />
            <Route path="/filiere" element={<Filieres />} />
            <Route path="/matieres" element={<Matieres />} />
            <Route path="/listeEtudiants" element={<ListeEtudiants />} />

            <Route path="/listeProfesseurs" element={<ListeProfesseurs />} />

            <Route path="*" element={<Navigate replace to="/" />} />
          </Route>
        </Routes>
      </Provider>
    </div>
  );
}

export default App;
